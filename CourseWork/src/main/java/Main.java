import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("AppView.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);

            Image img = new Image("/MachDefGeoIcon.png");
            primaryStage.getIcons().add(img);
            primaryStage.setTitle("Convex Hull Builder");
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}