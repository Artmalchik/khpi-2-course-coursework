import dao.MyCircle;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.persistence.Query;
import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApplicationController {
    @FXML
    Pane mainPane;
    @FXML
    Button checkPointModeSwitchButton;
    @FXML
    Label checkPointModeLabel;
    @FXML
    TextField textFieldAmountOfPoints;
    @FXML
    ProgressIndicator progressBar;

    private List<Circle> listOfCircles = new ArrayList<>();
    private List<Point> pointSet = new ArrayList<>();

    // Variables that are used to move points
    private double orgSceneX;
    private double orgSceneY;

    //Flag for checkPointMode
    private boolean checkPointModeON;

    private void readByteArrayFromVideoFile(File file, byte[] array) throws IOException {
        try {
            RandomAccessFile f = new RandomAccessFile(file.toString(), "r");
            f.readFully(array);
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading file " + ioe);
        }
    }

    @FXML
    public void readFromVideoFile() throws IOException {
        try {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(null);

            byte[] bytesFromFile = new byte[5000];

            if (file != null) {
                readByteArrayFromVideoFile(file, bytesFromFile);
            }

            int amountOfPoints = Integer.valueOf(textFieldAmountOfPoints.getText());
            byte[] listOfPoints = new byte[amountOfPoints * 2];

            int counter = 0;
            for (int i = 0; i < bytesFromFile.length; i++) {
                if (listOfPoints[listOfPoints.length - 1] != 0) {
                    break;
                }
                if (bytesFromFile[i] != 0) {
                    listOfPoints[counter] = bytesFromFile[i];
                    counter++;
                }
            }
            drawPoints(listOfPoints);
        }
        catch(Exception ex) {
            showNoDataError();
        }
    }

    private void drawPoints(byte[] listOfPoints) {
        //Choose the center of coordinate system
        double xTranslate = mainPane.getWidth() / 2;
        double yTranslate = mainPane.getHeight() / 2;

        //1.8 is coefficient for proper distribution of points
        for (int i = 0; i < listOfPoints.length - 1; i += 2) {
            Circle c = createCircle(listOfPoints[i] * 1.8 + xTranslate, listOfPoints[i + 1] * 1.8 + yTranslate);
            listOfCircles.add(c);
        }

        mainPane.getChildren().addAll(listOfCircles);
    }

    private Circle createCircle(double xCoordinate, double yCoordinate) {
        Circle circle = new Circle(xCoordinate, yCoordinate, 3);
        circle.setOnMousePressed(circleOnMousePressedEventHandler);
        circle.setOnMouseDragged(circleOnMouseDraggedEventHandler);
        circle.setOnMouseReleased(circleOnMouseDraggedReleaseEventHandler);
        circle.setCursor(Cursor.HAND);
        return circle;
    }

    @FXML
    public void drawConvexHull() {
        initializeListOfPoints();

        try {
            List<Point> convexHull = GrahamScan.getConvexHull(pointSet);

            for (int i = 0; i < convexHull.size() - 1; i++) {
                Line tmp = new Line(convexHull.get(i).getX(), convexHull.get(i).getY(), convexHull.get(i + 1).getX(),
                        convexHull.get(i + 1).getY());
                tmp.setOnMouseClicked(lineOnMouseClicked);
                tmp.setStrokeWidth(2);

                mainPane.getChildren().add(tmp);
            }
        } catch (Exception e) {
            showNoDataError();
        }
    }

    @FXML
    public void checkPoint() {
        if (checkPointModeON) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Point information");
            alert.setHeaderText(null);
            alert.setContentText("The point doesn't belong to convex hull.");
            alert.showAndWait();
        }
    }

    @FXML
    public void openTXT() throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);

        Scanner scanner = new Scanner(file);

        String[] stringArray = scanner.nextLine().split(" ");
        double[] array = Arrays.stream(stringArray).mapToDouble(Double::parseDouble).toArray();

        for (int i = 0; i < array.length - 1; i += 2) {
            Circle c = createCircle(array[i], array[i + 1]);
            listOfCircles.add(c);
        }

        mainPane.getChildren().addAll(listOfCircles);
    }

    @FXML
    public void saveToTXT() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(null);

        initializeListOfPoints();
        String pointSetAsString = "";
        for (Point point : pointSet) {
            pointSetAsString += point.getX() + " " + point.getY() + " ";
        }

        if (file != null) {
            saveFile(pointSetAsString, file);
        }
    }

    private void saveFile(String content, File file) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(ApplicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void openJSON() throws IOException, ParseException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(file));

        JSONObject jsonObject = (JSONObject) obj;

        JSONArray xCoordinates = (JSONArray) jsonObject.get("xCoordinates");
        JSONArray yCoordinates = (JSONArray) jsonObject.get("yCoordinates");

        for (int i = 0; i < xCoordinates.size(); i++) {
            Circle c = createCircle(Double.valueOf(xCoordinates.get(i).toString()), Double.valueOf(yCoordinates.get(i).toString()));
            listOfCircles.add(c);
        }

        mainPane.getChildren().addAll(listOfCircles);
    }

    @FXML
    public void saveToJSON() {
        JSONObject obj = new JSONObject();
        initializeListOfPoints();

        JSONArray xCoordinatesList = new JSONArray();
        JSONArray yCoordinatesList = new JSONArray();

        for (int i = 0; i < pointSet.size() - 1; i++) {
            xCoordinatesList.add(pointSet.get(i).getX());
            yCoordinatesList.add(pointSet.get(i).getY());
        }

        obj.put("xCoordinates", xCoordinatesList);
        obj.put("yCoordinates", yCoordinatesList);

        try {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showSaveDialog(null);

            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(obj.toJSONString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void showAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText(null);
        alert.setContentText(
                "The programm allows you to build a convex hull using data from a random videofile.\nCreated by Artem Malchenko");
        alert.showAndWait();
    }

    private void showNoDataError() {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("No data!");
        alert.setHeaderText(null);
        alert.setContentText("The program can't process the data that has been inputed, try again.");
        alert.showAndWait();
    }

    @FXML
    public void buildConvexHullStepByStep() {
        try {
            initializeListOfPoints();
            new buildConvexHullStepByStep().start();
        } catch (Exception e) {
            showNoDataError();
        }
    }

    private void initializeListOfPoints() {
        pointSet.clear();
        for (Circle tmp : listOfCircles) {
            pointSet.add(new Point((int) tmp.getCenterX() + (int) tmp.getTranslateX(),
                    (int) tmp.getCenterY() + (int) tmp.getTranslateY()));
        }
    }

    @FXML
    public void checkPointModeSwitch() {
        checkPointModeON = !checkPointModeON;

        if (checkPointModeON) {
            checkPointModeSwitchButton.setText("ON");
        } else {
            checkPointModeSwitchButton.setText("OFF");
        }
    }

    @FXML
    public void cleanCanvas() {
        mainPane.getChildren().clear();
        listOfCircles.clear();
        pointSet.clear();
        textFieldAmountOfPoints.setText("");
    }

    @FXML
    private void savePointsToDB() {
        new savePointsToDBService().start();
    }

    @FXML
    private void getPointsFromDB() {
       new getPointsFromDBService().start();
    }

    @FXML
    public void showHelp() throws IOException, URISyntaxException {
        String url = "C:/Users/Artmal/git/khpi-2-course-coursework/CourseWork/src/main/resources/userManual/about.html";
        File htmlFile = new File(url);
        Desktop.getDesktop().browse(htmlFile.toURI());

//        URL url = getClass().getClassLoader().getResource("about.html");
//        File htmlFile = new File(url.toString());
//        Desktop.getDesktop().browse(htmlFile.toURI());
    }

    @FXML
    private void saveReport() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(null);

        List<String> report = new ArrayList<>();
        report.add("Task: build convex hull from given points");
        report.add("Point Set: " + pointSet.toString());
        report.add("Starting point: " + GrahamScan.getLowestPoint(pointSet));
        report.add("Sorted sequence of points by polar angles: " + GrahamScan.getConvexHull(pointSet));

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
            for(String s : report) {
                writer.write(s);
                writer.newLine();
                writer.newLine();
            }

            writer.write("Tabular representation of solution:");
            writer.newLine();
            List<Point> result = GrahamScan.getConvexHull(pointSet);
            for(Point point : result) {
                writer.write(" | " + point.getX() + " | " + point.getY() + " | ");
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private EventHandler<MouseEvent> circleOnMousePressedEventHandler = (t) -> {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
        };

    private EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = (t) -> {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;

            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();

            ((Circle) (t.getSource())).setTranslateX(((((Circle) (t.getSource())).getTranslateX()) + offsetX));
            ((Circle) (t.getSource())).setTranslateY(((((Circle) (t.getSource())).getTranslateY()) + offsetY));
            ((Circle) (t.getSource())).setFill(Color.RED);
        };

    private EventHandler<MouseEvent> circleOnMouseDraggedReleaseEventHandler = (t) -> ((Circle) (t.getSource())).setFill(Color.BLACK);

    private EventHandler<MouseEvent> lineOnMouseClicked = (t) -> {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Point information");
            alert.setHeaderText(null);
            alert.setContentText("The point belongs to convex hull.");
            alert.showAndWait();
            t.consume();
        };

    private class getPointsFromDBService extends Service<Void> {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    progressBar.setVisible(true);
                    progressBar.setProgress(0.3);
                    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
                    Session mySession = sessionFactory.openSession();
                    mySession.beginTransaction();
                    progressBar.setProgress(0.6);

                    Query selectXCoordinate = mySession.createQuery("select xCoordinate from MyCircle");
                    Query selectYCoordinate = mySession.createQuery("select yCoordinate from MyCircle");

                    List xCoordinates = selectXCoordinate.getResultList();
                    List yCoordinates = selectYCoordinate.getResultList();
                    mySession.close();

                    listOfCircles.clear();

                    for(int i = 0; i < xCoordinates.size(); i++) {
                        createCircle(Double.valueOf(xCoordinates.get(i).toString()), Double.valueOf(yCoordinates.get(i).toString()));
                        Circle tmp = createCircle(Double.valueOf(xCoordinates.get(i).toString()), Double.valueOf(yCoordinates.get(i).toString()));
                        listOfCircles.add(tmp);
                    }

                    Platform.runLater(() -> mainPane.getChildren().addAll(listOfCircles));
                    initializeListOfPoints();
                    progressBar.setProgress(1);
                    Thread.sleep(1000);
                    progressBar.setVisible(false);
                    progressBar.setProgress(0);
                    return null;
                }
            };
        }
    }

    private class savePointsToDBService extends Service<Void> {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    progressBar.setVisible(true);
                    progressBar.setProgress(0.3);

                    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
                    Session mySession = sessionFactory.openSession();
                    mySession.beginTransaction();

                    String hql = String.format("delete from %s", "MyCircle");
                    Query query = mySession.createQuery(hql);
                    query.executeUpdate();

                    progressBar.setProgress(0.6);

                    initializeListOfPoints();

                    for (Point point : pointSet) {
                        MyCircle tmp = new MyCircle(point.getX(), point.getY());
                        mySession.save(tmp);
                    }

                    mySession.getTransaction().commit();
                    mySession.close();

                    progressBar.setProgress(1);
                    Thread.sleep(1000);
                    progressBar.setVisible(false);
                    progressBar.setProgress(0);
                    return null;
                }
            };
        }
    }

    private class buildConvexHullStepByStep extends Service<Void> {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    List<Point> convexHull = GrahamScan.getConvexHull(pointSet);
                    for (int i = 0; i < convexHull.size() - 1; i++) {
                        Line tmp = new Line(convexHull.get(i).getX(), convexHull.get(i).getY(), convexHull.get(i + 1).getX(),
                                convexHull.get(i + 1).getY());
                        tmp.setOnMouseClicked(lineOnMouseClicked);
                        tmp.setStrokeWidth(2);

                        Platform.runLater(() -> mainPane.getChildren().add(tmp));

                        Thread.sleep(500);
                    }
                    return null;
                }
            };
        }
    }
}